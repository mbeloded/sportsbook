package com.courses.sportsbook.entities;

/**
 * Created by Scorp on 21.04.2014.
 */
public class User {
    private int _id;
    private String userName;
    private String password;
    private String email;

    public User(int _id, String userName, String password, String email) {
        this._id = _id;
        this.userName = userName;
        this.password = password;
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int get_id() {
        return _id;
    }

    @Override
    public String toString() {
        return "User{" +
                "_id=" + _id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
