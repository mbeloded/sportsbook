package com.courses.sportsbook.UI.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.courses.sportsbook.R;

/**
 * Created by Scorp on 21.04.2014.
 */
public class HomeScreenFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_screen_fragment,container,false);
    }
}