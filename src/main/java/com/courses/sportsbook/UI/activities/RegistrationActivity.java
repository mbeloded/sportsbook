package com.courses.sportsbook.UI.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.courses.sportsbook.R;
import com.courses.sportsbook.database.DatabaseHelper;

/**
 * Created by Scorp on 20.04.2014.
 */
public class RegistrationActivity extends Activity {
    private EditText etRegisterLogin, etRegisterPass, etConfirmPass, etEmailAddress;
    private CheckBox checkBoxTerms;
    private Button btnRegisterNewUser, btnBack;
    private DatabaseHelper databaseHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        databaseHelper = new DatabaseHelper(this);
        initializeViews();
    }

    private void initializeViews() {
        etRegisterLogin = (EditText) findViewById(R.id.etRegisterLogin);
        etRegisterPass = (EditText) findViewById(R.id.etRegisterPass);
        etConfirmPass = (EditText) findViewById(R.id.etConfirmPass);
        etEmailAddress = (EditText) findViewById(R.id.etEmailAddress);
        checkBoxTerms = (CheckBox) findViewById(R.id.checkBoxTerms);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnRegisterNewUser = (Button) findViewById(R.id.btnRegisterNewUser);
        btnRegisterNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkBoxTerms.isChecked()) {
                    Toast.makeText(getApplicationContext(), "You must accept Terms and Conditions to register", Toast.LENGTH_SHORT).show();
                    return;
                }
                String userName = etRegisterLogin.getText().toString();
                String password = etRegisterPass.getText().toString();
                String confirmPassword = etConfirmPass.getText().toString();
                String email = etEmailAddress.getText().toString();
                if (checkInput(userName, password, confirmPassword, email)) {
                    return;
                }
                if (checkUserAndPass(userName, email)) return;
                databaseHelper.insertUser(userName, email, password);
                Toast.makeText(getApplicationContext(), "Account Created!", Toast.LENGTH_SHORT).show();
                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });
    }

    private boolean checkInput(String userName, String password, String confirmPassword, String email) {
        if (userName.equals("") || password.equals("") || confirmPassword.equals("") || email.equals("")) {
            Toast.makeText(getApplicationContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (userName.length() < 4) {
            Toast.makeText(getApplicationContext(),"Username must be at least 4 characters long",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (password.length() < 4) {
            Toast.makeText(getApplicationContext(),"Password must be at least 4 characters long",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (!password.equals(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "Passwords must match", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private boolean checkUserAndPass(String userName, String email) {
        if (databaseHelper.isUserTaken(userName)) {
            Toast.makeText(getApplicationContext(), "This username is already taken\nChoose another one", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (databaseHelper.isEmailTaken(email)) {
            Toast.makeText(getApplicationContext(),"This email is already taken\nChoose another one",Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}