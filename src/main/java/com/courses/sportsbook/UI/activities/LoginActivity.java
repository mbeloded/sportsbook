package com.courses.sportsbook.UI.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.courses.sportsbook.R;
import com.courses.sportsbook.database.DatabaseHelper;
import com.courses.sportsbook.entities.User;
import sun.util.resources.LocaleNames_da;

import java.util.List;

/**
 * Created by Scorp on 20.04.2014.
 */
public class LoginActivity extends Activity {
    private Switch switchSaveLogin, switchSavePassword;
    private EditText etLogin, etPassword;
    private Button btnRegister, btnLogin, btnUpdateLV;
    private ListView lvRegisteredUsers;
    private DatabaseHelper databaseHelper;
    private SharedPreferences sharedPreferences;

    private static final String KEY_PASS = "PASSWORD";
    private static final String KEY_LOGIN = "LOGIN";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        databaseHelper = new DatabaseHelper(this);
        initializeViews();
    }
    private void initializeViews() {
        switchSaveLogin = (Switch) findViewById(R.id.switchSaveLogin);
        switchSavePassword = (Switch) findViewById(R.id.switchSavePassword);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        lvRegisteredUsers = (ListView) findViewById(R.id.lvRegisteredUsers);
        btnUpdateLV = (Button) findViewById(R.id.btnUpdateLV);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrationActivity = new Intent(getApplicationContext(), RegistrationActivity.class);
                startActivity(registrationActivity);
                finish();
            }
        });
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = etLogin.getText().toString();
                String password = etPassword.getText().toString();
                String storedPassword = databaseHelper.getUserPassword(login);
                if (storedPassword.equals(password)) {
                    Toast.makeText(getApplicationContext(),"Login Successful",Toast.LENGTH_SHORT).show();
                    Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(mainActivity);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),"Username or password does not match",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnUpdateLV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> allUsers = databaseHelper.getAllUsers();
                if (allUsers == null || allUsers.size() == 0) {
                    return;
                }
                ArrayAdapter<User> arrayAdapter = new ArrayAdapter<User>(getApplicationContext(),R.layout.listview_item,allUsers);
                lvRegisteredUsers.setAdapter(arrayAdapter);
                lvRegisteredUsers.invalidateViews();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (switchSavePassword.isChecked()) {
            String password = etPassword.getText().toString();
            outState.putString(KEY_PASS,password);
        }
        if (switchSaveLogin.isChecked()) {
            String login = etLogin.getText().toString();
            outState.putString(KEY_LOGIN,login);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etLogin.setText(savedInstanceState.getString(KEY_LOGIN));
        etPassword.setText(savedInstanceState.getString(KEY_PASS));
    }

    @Override
    protected void onStop() {
        super.onStop();
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (switchSaveLogin.isChecked()) {
            edit.putString(KEY_LOGIN,etLogin.getText().toString());
            edit.commit();
        }
        if (switchSavePassword.isChecked()) {
            edit.putString(KEY_PASS,etPassword.getText().toString());
            edit.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getPreferences(MODE_PRIVATE);
        if (switchSavePassword.isChecked()) {
            etPassword.setText(sharedPreferences.getString(KEY_PASS,""));
        }
        if (switchSaveLogin.isChecked()) {
            etLogin.setText(sharedPreferences.getString(KEY_LOGIN,""));
        }
    }
}