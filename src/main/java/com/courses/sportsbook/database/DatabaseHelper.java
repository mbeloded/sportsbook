package com.courses.sportsbook.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.courses.sportsbook.entities.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Scorp on 20.04.2014.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TABLE_NAME = "REGISTERED_USERS";
    private static final String DB_NAME = "users.db";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_ID = "_id";

    private static final String CREATE_DATABASE = "CREATE TABLE " + TABLE_NAME + "( " + COLUMN_ID + " " +
            "INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_USERNAME + " TEXT, " + COLUMN_EMAIL + " " +
            "TEXT, " + COLUMN_PASSWORD + " TEXT);";

    private static final int DATABASE_VERSION = 1;


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insertUser(String userName,String email,String password) {
        ContentValues newValues = new ContentValues();
        newValues.put(COLUMN_USERNAME,userName);
        newValues.put(COLUMN_EMAIL,email);
        newValues.put(COLUMN_PASSWORD,password);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, newValues);
        db.close();
    }

    public int deleteUser(String userName) {
        SQLiteDatabase db = getWritableDatabase();
        String where = COLUMN_USERNAME+"=?";
        int deletedUsers = db.delete(TABLE_NAME, where, new String[]{userName});
        return deletedUsers;
    }

    public boolean isUserTaken(String userName) {
        SQLiteDatabase db = getWritableDatabase();
        boolean result = false;
        Cursor query = db.query(TABLE_NAME, null, " " + COLUMN_USERNAME + "=?", new String[]{userName}, null, null, null);
        if (query.getCount() < 1 ) {
            result = false;
        } else  {
            result = true;
        }
        db.close();
        query.close();
        return result;
    }
    public boolean isEmailTaken(String email) {
        SQLiteDatabase db = getWritableDatabase();
        boolean result = false;
        Cursor query = db.query(TABLE_NAME, null, " " + COLUMN_EMAIL + "=?", new String[]{email}, null, null, null);
        if (query.getCount() < 1) {
            result = false;
        } else {
            result = true;
        }
        db.close();
        query.close();
        return result;
    }

    public String getUserPassword(String userName) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, " " + COLUMN_USERNAME + "=?", new String[]{userName}, null, null, null);
        if (cursor.getCount() < 1) {
            cursor.close();
            return "No such user";
        }
        cursor.moveToFirst();
        String password = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
        db.close();
        cursor.close();
        return password;
    }

    public void updateEntry(String userName, String email, String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USERNAME,userName);
        contentValues.put(COLUMN_EMAIL,email);
        contentValues.put(COLUMN_PASSWORD,password);

        String where = COLUMN_USERNAME+"=?";
        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_NAME,contentValues,where,new String[]{userName});
        db.close();
    }

    public List<User> getAllUsers() {
        List<User> returnList = new ArrayList<User>();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int _id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                String userName = cursor.getString(cursor.getColumnIndex(COLUMN_USERNAME));
                String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                String password = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
                User user = new User(_id, userName, password, email);
                returnList.add(user);
            }
        }
        db.close();
        return returnList;
    }
}
